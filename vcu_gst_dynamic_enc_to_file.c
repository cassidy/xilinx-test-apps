#include <gst/gst.h>
#include <glib.h>
#include <getopt.h>
#include <ctype.h>
#include <stdlib.h>
//Globals
int targetBitrate = 5000;

static gboolean
bus_call (GstBus     *bus,
          GstMessage *msg,
          gpointer    data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

gboolean event_element_property_change (gpointer data) {
    static int decreaseBitrate = 0;
 
    GstElement *encoder = (GstElement *) data;
    g_print (" Target bitrate = %d \n",targetBitrate);
    guint value = ((targetBitrate) % 100000);
    g_print (" Changing video encoder element property - bitrate=%d \n",value);
    g_object_set(G_OBJECT (encoder), "target-bitrate", value, NULL);

    if (targetBitrate > 95000)
    	decreaseBitrate = 1;
    else if (targetBitrate < 10000)
	decreaseBitrate = 0;

    if (decreaseBitrate)
	    targetBitrate = targetBitrate - 5000;
    else
	    targetBitrate = targetBitrate + 5000;

    return TRUE;

}

int isNumber(char number[])
{
    int i = 0;

    //checking for negative numbers
    if (number[0] == '-')
        i = 1;
    for (; number[i] != 0; i++)
    {
        //if (number[i] > '9' || number[i] < '0')
        if (!isdigit(number[i]))
            return 0;
    }
    return 1;
}

int main (int   argc, char *argv[])
{
  GMainLoop *loop;
  GstElement *pipeline, *source, *sink, *encoder, *capsfilter, *enc_queue, *dec_queue;
  GstCaps *src_caps; 
  GstBus *bus;
  int width = 3840, height = 2160, framerate = 30;
  int controlRate = 2, gopLength = 30, bFrames = 0, maxBitrate = 5000;
  int timeInterval = 30;
  char *outputPath = "/run/op.h264";
  char *encoderType = "avc";
 
  guint bus_watch_id;

  while (1)
  {
	  int current_arg = 0;
	  static struct option long_options[] =
	  {
		  /* These options don’t set a flag.
		     We distinguish them by their indices. */	  
		  {"width",  required_argument, 0, 'w'},
		  {"height",  required_argument, 0, 'h'},
		  {"framerate", required_argument, 0, 'f'},
		  {"control-rate", required_argument, 0, 'c'},
		  {"b-frames", required_argument, 0, 'b'},
		  {"target-bitrate", required_argument, 0, 'r'},
		  {"max-bitrate", required_argument, 0, 'm'},
		  {"time-interval", required_argument, 0, 't'},
		  {"output-path", required_argument, 0, 'o'},
		  {"gop-length", required_argument, 0, 'g'},
		  {"encoder-type", required_argument, 0, 'e'},
		  {"help", no_argument, 0, 'u'},
		  {0, 0, 0, 0}
	  };
	  /* getopt_long stores the option index here. */
	  int option_index = 0;

	  current_arg = getopt_long (argc, argv, "w:h:f:c:r:t:b:o:g:e:m:u",
			  long_options, &option_index);

	  /* Detect the end of the options. */
	  if (current_arg == -1)
		  break;

	  switch (current_arg)
	  {
		  case 'u':
			  g_print ("vcu_gst_dynamic_enc_to_file -w <width_value> -h <height_value> -e <encoder_type> -f <frame_rate_value> -r <starting_target_bitrate> -b <number_of_bframes> -c <rate_control_mode> -t <time_period_to_trigger_change_event> -g <gop_length> -o <output_path>\n");
			  g_print ("or vcu_gst_dynamic_enc_to_file --width <width_value> --height <height_value> --encoder <encoder_type> --framerate <frame_rate_value> --control-rate <rate_control_mode> --time-interval <time_period_to_trigger_change_event> --gop-length <gop_length_value> --target-bitrate <starting_target_bitrate> --b-frames <number_of_bframes> --output-path <output_path> \n");
			  g_print ("e.g. vcu_gst_dynamic_enc_to_file -w 3840 -h 2160 -e avc -f 30 -c 2 -t 30 -g 30 -o /run/op.h264\n");
			  g_print ("e.g. for VBR vcu_gst_dynamic_enc_to_file --width 3840 --height 2160 --encoder-type avc --framerate 30 --control-rate 1 --time-interval 30 --gop-length 30 --target-bitrate <starting_target_bitrate> --max-bitrate <max_bitrate_to_go_upto> --output-path /run/op.h264 \n");
			  return 0;
			  break;
		  case 'w':
			  g_print ("Width = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
				width = atoi(optarg);
			  break;

		  case 'h':
			  g_print ("Height = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			        height =  atoi(optarg);
			  break;

		  case 'f':
			  g_print ("Framerate = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	framerate = atoi(optarg);
			  break;
		  case 'c':
			  g_print ("Control-rate = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	controlRate = atoi(optarg);
			  break;
		  case 'r':
			  g_print ("Target bitrate = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	targetBitrate = atoi(optarg);
			  break;
		  case 't':
			  g_print ("Time period to trigger event = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	timeInterval = atoi(optarg);
			  break;
		  case 'b':
			  g_print ("Number of b-frames  = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	bFrames = atoi(optarg);
			  break;
		
		  case 'o':
			  g_print ("Output path = `%s'\n", optarg);
			  outputPath = optarg;
			  break;
		  case 'g':
			  g_print ("Gop-Length = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	gopLength = atoi(optarg);
			  break;
		  case 'm':
			  g_print ("max-bitrate = `%s'\n", optarg);
			  if (!isNumber(optarg))
				g_print("Invalid value, please enter in digits, app will use now defaults\n");
			  else
			  	maxBitrate = atoi(optarg);
			  break;
		  case 'e':
			  g_print ("Encoder-type = `%s'\n", optarg);

    			  if ((g_strcmp0 (optarg, "avc") != 0) && (g_strcmp0 (optarg, "hevc") !=0 ))
				g_print("Invalid value, please enter either avc or hevc, app will use now defaults");
			  else
			  	encoderType = optarg;
			  break;
		  case '?':
			  /* getopt_long already printed an error message. */
			  break;

		  default:
			  abort ();
	  }
  }

  /* Initialization */
  gst_init(&argc, &argv);
  
  loop = g_main_loop_new(NULL, FALSE);
  
  /* Create Gstreamer elements */
  pipeline = gst_pipeline_new("video-player");
  source = gst_element_factory_make("v4l2src", "HDMI Rx");
  capsfilter = gst_element_factory_make("capsfilter",  "SRC Caps filter");
  if (!g_strcmp0(encoderType,"avc"))
	  encoder = gst_element_factory_make("omxh264enc",  "OMX H264 Encoder");
  else
	  encoder = gst_element_factory_make("omxh265enc",  "OMX H265 Encoder");

  enc_queue = gst_element_factory_make("queue",  "Encoder Queue");
  sink = gst_element_factory_make("filesink", "FileSink");
  
  if ( !pipeline || !source || !sink || !capsfilter || !encoder || !enc_queue )
  {
	  g_printerr("elements could not be created \n");
	  return -1;
  }
  
  /* set element properties */
  
  g_object_set(G_OBJECT (source), "io-mode", 4, NULL);
  g_object_set(G_OBJECT (source), "device", "/dev/video1", NULL);
  src_caps = gst_caps_new_simple ("video/x-raw",
		  "width",  G_TYPE_INT, width, "height", G_TYPE_INT,
		  height, "format", G_TYPE_STRING, "NV12", "framerate",
		  GST_TYPE_FRACTION, framerate, 1, NULL); 

  GST_DEBUG ("Caps for src capsfilter %s\n",gst_caps_to_string(src_caps));
  g_object_set (G_OBJECT (capsfilter),  "caps",  src_caps, NULL);
  g_object_set(G_OBJECT (encoder), "ip-mode", 2,  NULL);
  g_object_set(G_OBJECT (encoder), "target-bitrate", targetBitrate,  NULL);
  g_object_set(G_OBJECT (encoder), "b-frames", bFrames,  NULL);
  g_object_set(G_OBJECT (encoder), "control-rate", controlRate,  NULL);
  g_object_set(G_OBJECT (sink), "location", outputPath, NULL); 
  
  if (controlRate == 1) {
  	g_object_set(G_OBJECT (encoder), "max-bitrate", maxBitrate,  NULL);
	g_print("Using width = %d height = %d framerate = %d codec = %s target-bitrate = %d control-rate = %d b-frames = %d location = %s\n max-bitrate = %d", width, height, framerate, encoderType, targetBitrate, controlRate, bFrames, outputPath, maxBitrate)  ;
   /* add a message handler */
 } else {
	g_print("Using width = %d height = %d framerate = %d codec = %s target-bitrate = %d control-rate = %d b-frames = %d location = %s\n", width, height, framerate, encoderType, targetBitrate, controlRate, bFrames, outputPath)  ;
  }


  bus = gst_pipeline_get_bus(GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref(bus);
  
  /* Add elements into pipeline */
  
   gst_bin_add_many (GST_BIN (pipeline), source, capsfilter, encoder, enc_queue, sink, NULL);
   
  /* link the elements */
  gst_element_link_many(source, capsfilter, encoder, enc_queue, sink, NULL);
  
  /* Set the pipeline to "playing"*/ 
  g_print ("Now playing: %s\n", argv[1]);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);
  
   /* Iterate */
  g_print ("Running...\n");
 
  /* Change source Element property after "timeInterval" sec */
  g_timeout_add_seconds (timeInterval, (GSourceFunc)event_element_property_change, encoder);
  
  g_main_loop_run (loop);


  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping playback\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Deleting pipeline\n");
  gst_object_unref (GST_OBJECT (pipeline));
  g_source_remove (bus_watch_id);
  g_main_loop_unref (loop);

  return 0;
}
